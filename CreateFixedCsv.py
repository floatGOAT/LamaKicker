# -*- coding: utf-8 -*-
"""
Редактор Spyder

Это временный скриптовый файл.
"""
import pandas as pd

def sortByLength(inputStr):
        return len(inputStr)
    
def sortByAlphabet(inputStr):
        return inputStr[0]
    
clubs_france = set()
clubs_germany = set()
clubs_italy = set()
clubs_spain = set()
clubs_england = set()

clubs = [clubs_england, clubs_france, clubs_germany, clubs_italy, clubs_spain]

#clubs_spain = set()   
seasons = ['34','45','56', '67','78','89','910', '1011', '1112' ,'1213','1314','1415','1516','1617','1718','1819']
#seasons.sort(key=sortByLength)
countries = ['england','france','germany','italy','spain']
#countries.sort(key=sortByAlphabet)
leagues = ['Premier League','Ligue 1','Bundesliga','Serie A','LaLiga']

def checkNameOfClub(country, season, c):
    #for transfers
    global clubs
    tr_path='data/ready/'+country+'/ready-'+country+'-'+season+'.csv'
    tr_table = pd.read_csv(tr_path, sep=',')
    for i in range(len(tr_table.Club)):
        clubs[c].add(tr_table.Club[i])
    #for results
    res_path='data/results/'+country+'/r-'+country+'-'+season+'.csv'
    res_table = pd.read_csv(res_path, sep=',')
    for j in range(len(res_table.Club)):
        clubs[c].add(res_table.Club[j])
    
    
def superDuper(country, season, league):
    from_path = 'data/raw/'+country+'/'+country+'-'+season+'.csv'
    table = pd.read_csv(from_path, sep=',')
    table.drop('Club', axis=1, inplace=True)
    table.rename(columns={'Club.1':'Club'}, inplace=True)
    table.drop('#', 1, inplace=True)
    table=table[table.Competition==league]
    nums = [i for i in range(len(table.Competition==league))]
    table.index=nums
    formatMoneyToFloat(table.Income)
    formatMoneyToFloat(table.Expenditures)
    formatMoneyToFloat(table.Balance)
    to_path = 'data/ready/'+country+'/ready-'+country+'-'+season+'.csv'
    table.to_csv(to_path, sep=',', header=True)
    
def formatMoneyToFloat(arg):
    money_info = list()
    for i in range(arg.count()):
        if arg[i]=='-' or arg[i].startswith('+-'):
            arg[i]=0
            money_info.append(arg[i])
        else:
            arg[i]=arg[i].replace(',','.')
            money_info.append(arg[i].split(' '))
            money_info[i][0]=float(money_info[i][0])
            if money_info[i][1]=='Th.':
                money_info[i][0]/=1e3
                money_info[i][1]='Mill.'
            arg[i]=money_info[i][0]

def tipoMain():
    global clubs
    for c in range(len(clubs)):
        for i in range(len(seasons)):
            checkNameOfClub(countries[c], seasons[i], c)
        clubs[c]=sorted(clubs[c])
        for i in clubs[c]:
            print(i)

def ChangeNameOfClub(country, season):
    tr_path='data/ready/'+country+'/ready-'+country+'-'+season+'.csv'
    tr_table = pd.read_csv(tr_path, sep=',')
    if tr_table.columns[0]=='Unnamed: 0':
        tr_table.drop('Unnamed: 0', 1, inplace=True)
    for i in range(len(tr_table.Club)):
        VkusnoSwitch(tr_table.Club, i, country)
    tr_table.to_csv(tr_path, sep=',', header=True, index=False)    
    #for results
    res_path='data/results/'+country+'/r-'+country+'-'+season+'.csv'
    res_table = pd.read_csv(res_path, sep=',')
    if res_table.columns[0]=='#':
        res_table.drop('#', 1, inplace=True)
    if res_table.columns[0]=='Unnamed: 0':
        res_table.drop('Unnamed: 0', 1, inplace=True)
    if res_table.columns[0]=='Unnamed: 0.1':
        res_table.drop('Unnamed: 0.1', 1, inplace=True)   
    for j in range(len(res_table.Club)):
        VkusnoSwitch(res_table.Club, j, country)
    res_table.to_csv(res_path, sep=',', header=True, index=False)
            
            
             
def VkusnoSwitch(name, i, c):
            #
            #Spain
            #
        if c == 'spain':
            if name[i] == 'Alavés':
                name[i]='Alaves'
            if name[i] == 'Albacete':
                name[i]='Albacete'
            if name[i] == 'Albacete Balompi?':
                name[i]='Albacete'
            if name[i] == 'Athletic':
                name[i]='Athletic Bilbao'
            if name[i] == 'Athletic Bilbao':
                name[i]='Athletic Bilbao'
            if name[i] == 'Atl?tico Madrid':
                name[i]='Atletico Madrid'
            if name[i] == 'Atlético Madrid':
                name[i]='Atletico Madrid'
            if name[i] == 'C?diz CF':
                name[i]='Cadiz CF'
            if name[i] == 'CA Osasuna':
                name[i]='CA Osasuna'
            if name[i] == 'CD Legan?s':
                name[i]='CD Leganes'
            if name[i] == 'CD Leganés':
                name[i]='CD Leganes'
            if name[i] == 'CD Numancia':
                name[i]='CD Numancia'
            if name[i] == 'CD Tenerife':
                name[i]='CD Tenerife'
            if name[i] == 'Celta de Vigo':
                name[i]='Celta de Vigo'
            if name[i] == 'Cádiz CF':
                name[i]='Cadiz CF'
            if name[i] == 'Córdoba CF':
                name[i]='Cordoba CF'
            if name[i] == 'Dep. La Coruña':
                name[i]='Deportivo La Coruna'
            if name[i] == 'Deportivo Alav?s':
                name[i]='Alaves'
            if name[i] == 'Deportivo de La Coru?a':
                name[i]='Deportivo La Coruna'
            if name[i] == 'Elche CF':
                name[i]='Elche CF'
            if name[i] == 'Espanyol':
                name[i]='Espanyol'
            if name[i] == 'FC Barcelona':
                name[i]='FC Barcelona'
            if name[i] == 'Getafe':
                name[i]='Getafe'
            if name[i] == 'Getafe CF':
                name[i]='Getafe'
            if name[i] == 'Gimn?stic de Tarragona':
                name[i]='Gimnastic de Tarragona'
            if name[i] == 'Gimnàstic':
                name[i]='Gimnastic de Tarragona'
            if name[i] == 'Girona':
                name[i]='Girona'
            if name[i] == 'Girona FC':
                name[i]='Girona'
            if name[i] == 'Granada CF':
                name[i]='Granada CF'
            if name[i] == 'H?rcules CF':
                name[i]='Hercules CF'
            if name[i] == 'Levante':
                name[i]='Levante'
            if name[i] == 'Levante UD':
                name[i]='Levante'
            if name[i] == 'M?laga CF':
                name[i]='Malaga'
            if name[i] == 'Málaga CF':
                name[i]='Malaga'
            if name[i] == 'RCD Espanyol Barcelona':
                name[i]='Espanyol'
            if name[i] == 'RCD Mallorca':
                name[i]='RCD Mallorca'
            if name[i] == 'Racing':
                name[i]='Racing'
            if name[i] == 'Racing Santander':
                name[i]='Racing'
            if name[i] == 'Rayo Vallecano':
                name[i]='Rayo Vallecano'
            if name[i] == 'Real Betis':
                name[i]='Real Betis'
            if name[i] == 'Real Betis Balompi?':
                name[i]='Real Betis'
            if name[i] == 'Real Madrid':
                name[i]='Real Madrid'
            if name[i] == 'Real Murcia':
                name[i]='Real Murcia'
            if name[i] == 'Real Murcia CF':
                name[i]='Real Murcia'
            if name[i] == 'Real Sociedad':
                name[i]='Real Sociedad'
            if name[i] == 'Real Valladolid':
                name[i]='Real Valladolid'
            if name[i] == 'Real Valladolid CF':
                name[i]='Real Valladolid'
            if name[i] == 'Real Zaragoza':
                name[i]='Real Zaragoza'
            if name[i] == 'Recr. Huelva':
                name[i]='Recreativo Huelva'
            if name[i] == 'Recreativo Huelva':
                name[i]='Recreativo Huelva'
            if name[i] == 'SD Eibar':
                name[i]='SD Eibar'
            if name[i] == 'SD Huesca':
                name[i]='SD Huesca'
            if name[i] == 'Sevilla FC':
                name[i]='Sevilla FC'
            if name[i] == 'Sporting Gij?n':
                name[i]='Sporting Gijon'
            if name[i] == 'Sporting Gijón':
                name[i]='Sporting Gijon'
            if name[i] == 'UD Almer?a':
                name[i]='UD Almeria'
            if name[i] == 'UD Las Palmas':
                name[i]='UD Las Palmas'
            if name[i] == 'Valencia':
                name[i]='Valencia'
            if name[i] == 'Valencia CF':
                name[i]='Valencia'
            if name[i] == 'Villarreal':
                name[i]='Villarreal'
            if name[i] == 'Villarreal CF':
                name[i]='Villarreal'
            if name[i] == 'Xerez CD':
                name[i]='Xerez CD'
            #
            #England
            #
        elif c=='england':
            if name[i] == 'AFC Bournemouth':          
                name[i]='Bournemouth'
            if name[i] == 'Arsenal':
                name[i]='Arsenal'
            if name[i] == 'Arsenal FC':
                name[i]='Arsenal'
            if name[i] == 'Birmingham':
                name[i]='Birmingham'
            if name[i] == 'Birmingham City':
                name[i]='Birmingham'
            if name[i] == 'Blackburn':
                name[i]='Blackburn'
            if name[i] == 'Blackburn Rovers':
                name[i]='Blackburn'
            if name[i] == 'Blackpool FC':
                name[i] = 'Blackpool'
            if name[i] == 'Bolton Wanderers':
                name[i]='Bolton'
            if name[i] == 'Brighton & Hove Albion':
                name[i]='Brighton'
            if name[i] == 'Burnley FC':
                name[i]='Burnley'
            if name[i] == 'Charlton Athletic':
                name[i]='Charlton'
            if name[i] == 'Chelsea FC':
                #new_name[i]='Chelsea'
                name[i]='Chelsea'
            if name[i] == 'Derby':
                name[i]='Derby County'
            if name[i] == 'Everton FC':
                name[i]='Everton'
            if name[i] == 'Fulham FC':
                name[i]='Fulham'
            if name[i] == 'Huddersfield Town':
                name[i]='Huddersfield'
            if name[i] == 'Leicester City':
                name[i]='Leicester'
            if name[i] == 'Liverpool FC':
                name[i]='Liverpool'
            if name[i] == 'Man City':
                name[i]='Manchester City'
            if name[i] == 'Man Utd':
                name[i]='Manchester United'
            if name[i] == 'Middlesbrough FC':
                name[i]='Middlesbrough'
            if name[i] == 'Newcastle United':
                name[i]='Newcastle'
            if name[i] == 'Norwich City':
                name[i]='Norwich'
            if name[i] == 'Portsmouth											*':
                name[i]='Portsmouth'
            if name[i] == 'Portsmouth FC':
                name[i]='Portsmouth'
            if name[i] == 'QPR':
                name[i]='Queens Park Rangers'
            if name[i] == 'Reading FC':
                name[i]='Reading'
            if name[i] == 'Sheffield Utd.':
                name[i]='Sheffield United'
            if name[i] == 'Southampton FC':
                name[i]='Southampton'
            if name[i] == 'Spurs':
                name[i]='Tottenham Hotspur'
            if name[i] == 'Sunderland AFC':
                name[i]='Sunderland'
            if name[i] == 'Watford FC':
                name[i]='Watford'
            if name[i] == 'West Brom':
                name[i]='West Bromwich Albion'
            if name[i] == 'West Ham United':
                name[i]='West Ham'
            if name[i] == 'Wigan Athletic':
                name[i]='Wigan'
            if name[i] == 'Wolves':
                name[i]='Wolverhampton Wanderers'
            #
            #Italy
            #
        elif c=='italy':
            if name[i] == 'AC Cesena':
                name[i]='Cesena'
            if name[i] == 'AC Milan											*':
                name[i]='AC Milan'
            if name[i] == 'AC Parma':
                name[i]='Parma'
            if name[i] == 'AC Siena':
                name[i]='Siena'
            if name[i] == 'ACD Treviso':
                name[i]='Treviso'
            if name[i] == 'ACF Fiorentina':
                name[i]='Fiorentina'
            if name[i] == 'AS Bari':
                name[i]='Bari'
            if name[i] == 'Ascoli Calcio 1898':
                name[i]='Ascoli'
            if name[i] == 'Atalanta											*':
                name[i]='Atalanta'
            if name[i] == 'Atalanta BC':
                name[i]='Atalanta'
            if name[i] == 'Benevento Calcio':
                name[i]='Benevento'
            if name[i] == 'Bologna FC 1909':
                name[i]='Bologna'
            if name[i] == 'Brescia Calcio':
                name[i] = 'Brescia'
            if name[i] == 'Calcio Catania':
                name[i]='Catania'
            if name[i] == 'Chievo Verona											*':
                name[i]='Chievo Verona'
            if name[i] == 'Delfino Pescara 1936':
                name[i]='Pescara'
            if name[i] == 'FC Crotone':
                name[i]='Crotone'
            if name[i] == 'FC Internazionale':
                name[i]='Inter'
            if name[i] == 'FC Messina Peloro':
                name[i]='Messina Peloro'
            if name[i] == 'Fiorentina											*':
                name[i]='Fiorentina'
            if name[i] == 'Frosinone Calcio':
                name[i]='Frosinone'
            if name[i] == 'Genoa CFC':
                name[i]='Genoa'
            if name[i] == 'Inter Milan':
                name[i]='Inter'
            if name[i] == 'Juventus											*':
                name[i]='Juventus'
            if name[i] == 'Juventus FC':
                name[i]='Juventus'
            if name[i] == 'Lazio											*':
                name[i]='Lazio'
            if name[i] == 'Milan':
                name[i]='AC Milan'
            if name[i] == 'Milan AC':
                name[i]='AC Milan'
            if name[i] == 'Modena FC 2018':
                name[i]='Modena'
            if name[i] == 'Novara Calcio 1908':
                name[i]='Novara'
            if name[i] == 'Parma FC':
                name[i] = 'Parma'
            if name[i] == 'Parma											*':
                name[i]='Parma'
            if name[i] == 'Parma Calcio 1913':
                name[i]='Parma'
            if name[i] == 'Reggina											*':
                name[i]='Reggina'
            if name[i] == 'Reggina Calcio':
                name[i]='Reggina'
            if name[i] == 'SPAL 2013':
                name[i]='SPAL'
            if name[i] == 'SS Lazio':
                name[i]='Lazio'
            if name[i] == 'Sampdoria											*':
                name[i]='Sampdoria'
            if name[i] == 'Siena											*':
                name[i]='Siena'
            if name[i] == 'Torino											*':
                name[i]='Torino'
            if name[i] == 'Torino FC':
                name[i]='Torino'
            if name[i] == 'UC Sampdoria':
                name[i]='Sampdoria'
            if name[i] == 'US Lecce':
                name[i]='Lecce'
            if name[i] == 'US Sassuolo':
                name[i]='Sassuolo'
            if name[i] == 'Udinese Calcio':
                name[i]='Udinese'
            if name[i] == 'SS Lazio':
                name[i]='Lazio'
            if name[i] == 'Sampdoria                                            *':
                name[i]='Sampdoria'
            #
            # German
            #
        elif c=='germany':
            if name[i] == 'Alem. Aachen':
                name[i] = 'Alemannia Aachen'
            if name[i] == 'Borussia Dortmund' or name[i] == 'Bor. Dortmund' :
                name[i]='Borussia Dortmund'
            if name[i] == 'Bayern Munich' or name[i] == 'Bayern Munich' :
                name[i]='Bayern Munich'
            if name[i] == "Bor. M'gladbach" or name[i] == 'Borussia M?nchengladbach' :
                name[i]='Borussia MGB'
            if name[i] == "FC Schalke 04" or name[i] == 'FC Schalke 04' :
                name[i]='Schalke'
            if name[i] == "RB Leipzig" or name[i] == 'RB Leipzig' :
                name[i]='RB Leipzig'
            if name[i] == 'VfL Wolfsburg' or name[i] == 'VfL Wolfsburg' :
                name[i]='VFL Wolfsburg'
            if name[i] == 'VfB Stuttgart' or name[i] == 'VfB Stuttgart' :
                name[i]='VFB Stuttgart'
            if name[i] == 'Bayer 04 Leverkusen' or name[i] == 'Bay. Leverkusen' :
                name[i]='Bayer'
            if name[i] == '1.FSV Mainz 05' or name[i] == '1.FSV Mainz 05' :
                name[i]='FSV Mainz'
            if name[i] == 'TSG 1899 Hoffenheim' or name[i] == 'TSG Hoffenheim' :
                name[i]='Hoffenheim'
            if name[i] == 'SV Werder Bremen' or name[i] == 'Werder Bremen' :
                name[i]='Werder Bremen'
            if name[i] == 'Eintracht Frankfurt' or name[i] == 'E. Frankfurt' :
                name[i]='Eintracht Frankfurt'
            if name[i] == 'Hannover 96' or name[i] == 'Hannover 96' :
                name[i]='Hannover'
            if name[i] == 'SC Freiburg' or name[i] == 'SC Freiburg' :
                name[i]='Freiburg'
            if name[i] == 'Hertha BSC' or name[i] == 'Hertha BSC' :
                name[i]='Hertha'
            if name[i] == 'Fortuna D?sseldorf' or name[i] == 'F. DГјsseldorf' :
                name[i]='Fortuna Dusseldorf'
            if name[i] == 'FC Augsburg' or name[i] == 'FC Augsburg' :
                name[i]='Augsburg'
            if name[i] == '1.FC Nuremberg' or name[i] == '1.FC Nuremberg' :
                name[i]='Nuremberg'
            if name[i] == '1. FC K?ln' or name[i] == '1. FC KГ¶ln':
                name[i]='Koln'
            if name[i] == 'Hamburger SV' or name[i] == 'Hamburger SV' :
                name[i]='Hamburger SV'
            if name[i] == 'SC Paderborn 07' or name[i] == 'SC Paderborn' :
                name[i]='Paderborn'
            if name[i] == "1.FC K'lautern" or name[i] == '1.FC Kaiserslautern' or name[i] == "1.FC K'lautern											*":
                name[i]='Kaiserslautern'
            if name[i] == 'VfL Bochum' or name[i] == 'VfL Bochum' :
                name[i]='Bochum'
            if name[i] == 'Arminia Bielefeld' or name[i] == 'Arm. Bielefeld' :
                name[i]='Arminia Bielefeld'
            if name[i] == 'FC Energie Cottbus' :
                name[i]='Energie Cottbus'
            if name[i] == 'Karlsruher SC' or name[i] == 'Karlsruher SC' :
                name[i]='Karlsruher'
            if name[i] == 'FC Hansa Rostock' or name[i] == 'Hansa Rostock' :
                name[i]='Hansa Rostock'
            if name[i] == 'TSV 1860 Munich' or name[i] == '1860 Munich' :
                name[i]='TSV 1860 Munich'
            if name[i] == 'SpVgg Greuther F?rth' or name[i] == 'Greuther F?rth':
                name[i]='Greuther Ferth'
            if name[i] == 'E. Braunschweig':
                name[i]='Eintracht Braunschweig'
            if name[i] == 'F. D?sseldorf':
                name[i]='Fortuna Dusseldorf'
            if name[i] == 'FC Ingolstadt 04':
                name[i]='FC Ingolstadt'
            #
            #France
            #
        elif c=='france':
            if name[i] == 'AC Ajaccio											*':
                name[i]='AC Ajaccio'
            if name[i] == 'AS Nancy-Lorraine':
                name[i]='AS Nancy'
            if name[i] == 'AS Saint-?tienne':
                name[i]='Saint-Etienne'
            if name[i] == 'Saint-Étienne':
                name[i]='Saint-Etienne'
            if name[i] == 'EA Guingamp':
                name[i]='Guingamp'
            if name[i] == 'ES Troyes AC':
                name[i]='Troyes'
            if name[i] == 'FC ?vian Thonon Gaillard':
                name[i]='Evian'
            if name[i] == 'FC Girondins Bordeaux':
                name[i]='Bordeaux'
            if name[i] == 'FC Istres Ouest Provence':
                name[i]='FC Istres'
            if name[i] == 'Évian':
                name[i]='Evian'
            if name[i] == 'FC Nantes											*':
                name[i]='FC Nantes'
            if name[i] == 'FC Sochaux-Montb?liard':
                name[i]='FC Sochaux'
            if name[i] == 'FC Toulouse':
                name[i]='Toulouse'
            if name[i] == 'FCO Dijon':
                name[i]='Dijon'
            if name[i] == 'G. Bordeaux':
                name[i]='Bordeaux'
            if name[i] == 'Grenoble Foot 38':
                name[i]='Grenoble'
            if name[i] == 'HSC Montpellier':
                name[i]='Montpellier'
            if name[i] == 'Le Mans Union Club 72':
                name[i]='Le Mans'
            if name[i] == 'Le Mans UC 72':
                name[i]='Le Mans'
            if name[i] == 'Nîmes Olympique':
                name[i]='Nimes'
            if name[i] == 'N?mes Olympique':
                name[i]='Nimes'
            if name[i] == 'Olympique Marseille':
                name[i]='Marseille'
            if name[i] == 'Paris SG':
                name[i]='PSG'
            if name[i] == 'Paris Saint-Germain':
                name[i]='PSG'
            if name[i] == 'RC Strasbourg Alsace':
                name[i]='R. Strasbourg'
            if name[i] == 'RC Lens':
                name[i]='Lens'
            if name[i] == 'SC Bastia											*':
                name[i]='SC Bastia'
            if name[i] == 'Stade Reims':
                name[i]='Reims'
            if name[i] == 'Stade Rennais FC':
                name[i]='Stade Rennais'
   
def cccccchek():               
    for i in range(len(countries)):
        for j in range(len(seasons)):
            ChangeNameOfClub(countries[i], seasons[j])
           
def AddPlace():
    a = list()
#for i in range(len(countries)):
#    superDuper(countries[i], '1011', leagues[i])
            
cccccchek()           
tipoMain()

#for i in range(len(countries)):
#    for j in range(len(seasons)):
#        superDuper(countries[i], seasons[j], leagues[i])


    #table = pd.read_csv('1617.csv', sep=',')
    #table.drop('Club', axis=1, inplace=True)
    #table.rename[i](columns={'Club.1':'Club'}, inplace=True)
    #table.drop('#', 1, inplace=True)
    #table=table[table.Competition=='LaLiga']
    #nums = [i for i in range(len(table.Competition=='LaLiga'))]
    #table.index=nums
    #formatMoneyToFloat(table.Income)
    #formatMoneyToFloat(table.Expenditures)
    #formatMoneyToFloat(table.Balance)
    #table.to_csv('ready-spain-1617.csv', sep=',', header=True)