from tkinter import *
import rpy2.robjects as robj
from rpy2.robjects.packages import importr

def Analyse(team, balance):
	rinstance = robj.r;
	utils = importr("utils")
	utils.chooseCRANmirror(ind=1)
	utils.install_packages("tidyverse")
	utils.install_packages("lmtest")
	utils.install_packages("e1071")
	utils.install_packages("caret")

	rscript =  rinstance.source('analysisScript.R')
	res = rinstance.runAnalysis(team, balance)
	strng = ""
	for x in range(len(res)):
		pass
		strng = strng + res[x] + "\n"
	return strng

def Predict(event):
	global tbTeam
	global tbBalance
	global lbResult
	lbResult.configure(text=Analyse(tbTeam.get(), int(tbBalance.get())))

mainWindow = Tk()
mainWindow.title("LamaKicker")
mainWindow.geometry("450x300")

# Init frames
inputFrame = Frame(mainWindow, width=200, height=70)
buttonsFrame = Frame(mainWindow, width=200, height=30)
inputFrame.pack(side="top", expand=False)
buttonsFrame.pack(side="bottom", expand=False)

# Init inputs
Label(inputFrame, text="Insert team: ").grid(row=0, column=0, sticky=W)
tbTeam = Entry(inputFrame)
tbTeam.grid(row=0, column=1)
Label(inputFrame, text="Insert transfer balance, mill €: ").grid(row=1, column=0, sticky=W)
tbBalance = Entry(inputFrame)
tbBalance.grid(row=1, column=1)
Label(inputFrame, text="Result: ").grid(row=2, column=0, sticky=NW)
lbResult = Label(inputFrame, justify=LEFT)
lbResult.grid(row=2, column=1, sticky=W)

# Init buttons
btnPredict = Button(buttonsFrame, text="Predict")
btnPredict.bind("<Button-1>", Predict)
btnPredict.pack(side="right")

mainWindow.mainloop()