createPathToTransfers<- function(country, yearIndex) {
  fileName <- paste("ready-",
                country,
                "-",
                as.character(yearIndex),
                as.character(yearIndex + 1),
                ".csv",
                sep = "")
  path <- paste("data/ready/", country, "/", fileName, sep = "")
  return(path)
}

createPathToResults<- function(country, yearIndex) {
  fileName <- paste("r-",
                    country,
                    "-",
                    as.character(yearIndex),
                    as.character(yearIndex + 1),
                    ".csv",
                    sep = "")
  path <- paste("data/results/", country, "/", fileName, sep = "")
  return(path)
}

library(tidyverse)
countries <- c("england", "france", "germany", "italy", "spain")
resultsColNames <- c("Club", "Matches", "Win", "Draw", "Lose", "Goals", "+/-", "Pts")

# Final dataset
ds <- NA

# Iterate by countries  
for (c in countries) {
  full.table <- NA
  
  # Iterate by years
  for (y in 3:18) {
    
    # Open transfers table for current country and year
    transfers.path <- createPathToTransfers(c, y)
    cur.transfers <- read.csv(transfers.path, sep = ",")
    
    # Open results table for current country and year
    results.path <- createPathToResults(c, y)
    cur.results <- read.csv(results.path, sep = ",")
    
    # Hull results table
    colnames(cur.results) <- resultsColNames
    cur.results %>%
      mutate(Place = rownames(cur.results)[cur.results$Club == Club]) ->
      cur.results
    
    # Join transfers and results table
    cur.transfers %>% 
      mutate(Season = paste(as.character(y), "/", as.character(y + 1), sep = "")) %>% 
      left_join(., cur.results, by = "Club") ->
      cur.table
    
    # Add to full country table
    if(is.na(full.table)) {
      full.table <- cur.table
    }
    else {
      full.table <- full_join(full.table, cur.table)
    }
  }

  # Write full country table to .csv
  final.path <- paste("data/final/final-", c, ".csv", sep = "")
  full.table %>% 
    write.csv(., final.path, sep = ",")
  
  # Add to final dataset
  if(is.na(ds)) {
    ds <- full.table
  }
  else {
    ds <- full_join(ds, full.table)
  }
}

# Write final dataset to CSV
ds %>% 
  write.csv(file = "data/final/dataset.csv", sep = ",")